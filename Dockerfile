# Select ubuntu as the base image
#FROM ubuntu:14.04
FROM ruby:2.1
MAINTAINER Andrew Pawloski (apawloski@gmail.com)

# Add WhadRU Project to the container
ADD ./ /whadru

# Set WORKDIR
WORKDIR /whadru

RUN bundle install

# We'll run on this port
EXPOSE 4567

# We want to automatically start whadru when container is run
ENTRYPOINT ["ruby", "whadru-rest.rb", "-o", "0.0.0.0"]